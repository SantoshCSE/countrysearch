package com.santosh.csa.utils;

/**
 * Created by sanbhara on 1/4/19.
 */

public abstract class AppConstant {
    public static final String KEY = "KEY";
    public static final String BASE_URL = "https://restcountries.eu/rest/v2/";
}
