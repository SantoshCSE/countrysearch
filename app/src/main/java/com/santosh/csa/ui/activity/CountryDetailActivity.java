package com.santosh.csa.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.santosh.csa.R;
import com.santosh.csa.db.CountryDataSource;
import com.santosh.csa.models.Country;
import com.santosh.csa.models.Currency;
import com.santosh.csa.models.Language;
import com.santosh.csa.utils.AppConstant;
import com.santosh.csa.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by sanbhara on 1/4/19.
 */

public class CountryDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_country_flag)
    ImageView imageViewFlag;
    @BindView(R.id.tv_country_name)
    TextView countryName;
    @BindView(R.id.tv_country_region)
    TextView countryRegion;
    @BindView(R.id.tv_country_capital)
    TextView countryCapital;
    @BindView(R.id.tv_country_population)
    TextView countryPopulation;
    @BindView(R.id.tv_country_native_name)
    TextView countryNativeName;
    @BindView(R.id.tv_country_demonym)
    TextView countryDemonym;
    @BindView(R.id.tv_country_calling_codes)
    TextView countryCallingCodes;
    @BindView(R.id.tv_country_currencies)
    TextView countryCurrencies;
    @BindView(R.id.tv_country_languages)
    TextView countryLanguages;
    @BindView(R.id.tv_country_timezones)
    TextView countryTimezones;
    @BindView(R.id.btn_save)
    Button saveButton;

    private CountryDataSource mCountryRepository;
    private Country country;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_county_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mCountryRepository = CountryDataSource.getInstance(this);
        country = getIntent().getParcelableExtra(AppConstant.KEY);
        if (country != null) {
            displayData(country);
        }
        if (!AppUtils.isNetworkAvailable(this)) {
            saveButton.setVisibility(View.GONE);
        } else {
            saveButton.setVisibility(View.VISIBLE);
        }
    }

    public void displayData(Country country) {
        SvgLoader.pluck()
                .with(this)
                .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
                .load(country.getFlag(), imageViewFlag);
        countryName.setText(country.getName());
        countryRegion.setText(country.getRegion());
        countryCapital.setText(country.getCapital());
        countryPopulation.setText(country.getPopulation());
        countryNativeName.setText(country.getNativeName());
        countryDemonym.setText(country.getDemonym());
        if (country.getCallingCodes() != null) {
            countryCallingCodes.setText(android.text.TextUtils.join(", ", country.getCallingCodes()));
        }

        String currencies = "";
        for (Currency currency : country.getCurrencies()) {
            if (!currencies.isEmpty()) currencies += ", ";
            currencies += currency.getName();
            if (currency.getSymbol() != null) currencies += " (" + currency.getSymbol() + ")";
        }
        countryCurrencies.setText(currencies);

        String languages = "";
        for (Language language : country.getLanguages()) {
            if (!languages.isEmpty()) languages += ", ";
            languages += language.getName();
        }
        countryLanguages.setText(languages);
        if (country.getTimezones() != null) {
            countryTimezones.setText(android.text.TextUtils.join(", ", country.getTimezones()));
        }
       }

    @OnClick(R.id.btn_save)
    void submitButton(View view) {
        mCountryRepository.saveCounty(country);
        saveButton.setOnClickListener(null);
        saveButton.setAlpha(.5f);
        AppUtils.showMessage(this, getString(R.string.msg_generic_success));
    }
}
