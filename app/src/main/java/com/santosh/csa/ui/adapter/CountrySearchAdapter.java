package com.santosh.csa.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.santosh.csa.R;
import com.santosh.csa.models.Country;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sanbhara on 1/4/19.
 */

public class CountrySearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
        implements Filterable {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private Context mContext;
    private List<Country> mCountryList;
    private List<Country> mCountryListForSearch;

    private boolean isLoadingVisible = false;
    private int loadingItemIndex = -1;
    private OnCountryClickListener mOnItemClickListener;

    public CountrySearchAdapter(Context context, OnCountryClickListener onItemClickListener) {
        this.mContext = context;
        this.mOnItemClickListener = onItemClickListener;
        this.mCountryList = new ArrayList<>();
    }

    /**
     * Add country to list
     *
     * @param countries
     */
    public void addCountryToList(List<Country> countries) {
        this.mCountryList.addAll(countries);
        mCountryListForSearch = new ArrayList<>(mCountryList);
    }

    /**
     * Clear list and notify
     */
    public void clearList() {
        this.mCountryList.clear();
        notifyDataSetChanged();
    }

    /**
     * Show loading of Country
     */
    public void showLoadingItem() {
        this.isLoadingVisible = true;
        this.mCountryList.add(null);
        this.loadingItemIndex = mCountryList.size() - 1;
        notifyItemInserted(this.loadingItemIndex);
    }

    /**
     * Hide the loading
     */
    public void hideLoadingItem() {
        if (isLoadingVisible) {
            this.isLoadingVisible = false;
            this.mCountryList.remove(this.loadingItemIndex);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_country, parent, false);
            return new CountryViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_loading, parent, false);
            return new LoadingViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CountryViewHolder) {
            CountryViewHolder countryViewHolder = (CountryViewHolder) holder;
            Country country = mCountryList.get(position);
            SvgLoader.pluck()
                    .with((Activity) mContext)
                    .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
                    .load(country.getFlag(), countryViewHolder.countryFlag);
            countryViewHolder.countryName.setText(country.getName());
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mCountryList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return this.mCountryList.size();
    }

    protected class LoadingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progress_bar)
        ProgressBar progressBar;

        LoadingViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    protected class CountryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.iv_country_flag)
        ImageView countryFlag;
        @BindView(R.id.tv_name)
        TextView countryName;

        CountryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onCountryClick(view, getAdapterPosition());
            }
        }
    }

    @Override
    public Filter getFilter() {
        return searchFilter;
    }

    /**
     * Handles item click on attached view.
     */
    public interface OnCountryClickListener {
        /**
         * Click listener for Country
         *
         * @param view
         * @param position
         */
        void onCountryClick(View view, int position);

        /**
         * Show no data found message
         */
        void showNotFoundMessage();
    }

    private Filter searchFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Country> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(mCountryListForSearch);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Country item : mCountryListForSearch) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mCountryList.clear();
            mCountryList.addAll((List) results.values);
            notifyDataSetChanged();
            if (mCountryList.size() == 0 && mOnItemClickListener != null) {
                mOnItemClickListener.showNotFoundMessage();
            }
        }
    };

}
