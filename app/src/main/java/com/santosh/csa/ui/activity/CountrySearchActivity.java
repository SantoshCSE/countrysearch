package com.santosh.csa.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.jakewharton.rxbinding.support.v7.widget.RxSearchView;
import com.santosh.csa.R;
import com.santosh.csa.db.CountryDataSource;
import com.santosh.csa.listener.CountryPresenterView;
import com.santosh.csa.models.Country;
import com.santosh.csa.presenter.CountryPresenter;
import com.santosh.csa.presenter.impl.CountryPresenterImpl;
import com.santosh.csa.ui.adapter.CountrySearchAdapter;
import com.santosh.csa.utils.AppConstant;
import com.santosh.csa.utils.AppUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by sanbhara on 1/4/19.
 */

public class CountrySearchActivity extends AppCompatActivity implements CountryPresenterView, CountrySearchAdapter.OnCountryClickListener {

    private CountryPresenter mCountryPresenter;
    private CountrySearchAdapter countryAdapter;
    private MenuItem searchItem;

    @BindView(R.id.root_layout)
    RelativeLayout rootLayout;
    @BindView(R.id.layout_zero)
    RelativeLayout zeroStateLayout;
    @BindView(R.id.recycler_view_county)
    RecyclerView recyclerViewCountry;

    private List<Country> mCountriesList = new ArrayList<>();
    private CountryDataSource mCountryRepository;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCountryPresenter = new CountryPresenterImpl(this);
        setContentView(R.layout.activity_country_search);
        ButterKnife.bind(this);
        mCountryRepository = CountryDataSource.getInstance(this);
        countryAdapter = new CountrySearchAdapter(this, this);
        recyclerViewCountry.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewCountry.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCountry.setAdapter(countryAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!AppUtils.isNetworkAvailable(this)) {
            loadOfflineData();
        }
    }

    @Override
    public void clearDataShowLoading() {
        zeroStateLayout.setVisibility(View.GONE);
        countryAdapter.clearList();
        countryAdapter.showLoadingItem();
    }

    @Override
    public void dismissLoadAndNotify() {
        zeroStateLayout.setVisibility(View.GONE);
        countryAdapter.hideLoadingItem();
        countryAdapter.notifyDataSetChanged();
    }

    @Override
    public void addDataToCountryList(List<Country> countries) {
        zeroStateLayout.setVisibility(View.GONE);
        this.mCountriesList = countries;
        countryAdapter.addCountryToList(countries);
    }

    @Override
    public void handleErrorScenario(Throwable throwable) {
        countryAdapter.hideLoadingItem();
        int msgId = R.string.msg_generic_error;
        if (throwable.getMessage().equals("HTTP 404 Not Found")) {
            msgId = R.string.msg_no_search_result;
        }
        AppUtils.showMessage(this, getString(msgId));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        RxSearchView.queryTextChangeEvents(searchView)
                .skip(1)
                .throttleLast(100, TimeUnit.MILLISECONDS)
                .debounce(200, TimeUnit.MILLISECONDS)
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(searchViewQueryTextEvent -> {
                    final boolean empty = TextUtils.isEmpty(searchViewQueryTextEvent.queryText());
                    return !empty;
                })
                .subscribe(searchViewQueryTextEvent -> {
                    String searchTerm = searchViewQueryTextEvent.queryText().toString();

                    if (searchViewQueryTextEvent.isSubmitted()) {
                        AppUtils.hideKeyboard(this,rootLayout);
                    } else {
                        if (AppUtils.isNetworkAvailable(this)) {
                            mCountryPresenter.search(searchTerm);
                        } else {
                            if (mCountriesList.size() == 0) {
                                loadOfflineData();
                            }
                            searchOfflineCountry(searchView);
                        }
                    }
                });

        return true;
    }

    @Override
    protected void onDestroy() {
        mCountryPresenter.cleanUp();
        countryAdapter = null;
        mCountryPresenter = null;
        super.onDestroy();
    }

    @Override
    public void onCountryClick(View view, int position) {
        Intent intent = new Intent(this, CountryDetailActivity.class);
        intent.putExtra(AppConstant.KEY, mCountriesList.get(position));
        startActivity(intent);
    }

    @Override
    public void showNotFoundMessage() {
        AppUtils.showMessage(this, getString(R.string.msg_generic_no_data));
    }

    /**
     * Load offline saved data
     */
    private void loadOfflineData() {
        new AsyncTask<Void, Void, List<Country>>() {

            @Override
            protected List<Country> doInBackground(Void... voids) {

                return mCountryRepository.getCountyList();
            }

            @Override
            protected void onPostExecute(List<Country> recipes) {
                mCountriesList = recipes;
                countryAdapter.addCountryToList(mCountriesList);
                countryAdapter.notifyDataSetChanged();
                if (mCountriesList.size() > 0) {
                    zeroStateLayout.setVisibility(View.GONE);
                }
            }
        }.execute();
    }

    /**
     * Search Saved Country offline
     *
     * @param searchView
     */
    private void searchOfflineCountry(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                countryAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }
}
