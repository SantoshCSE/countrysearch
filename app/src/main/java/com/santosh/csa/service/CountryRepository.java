package com.santosh.csa.service;


import com.santosh.csa.models.Country;

import java.util.List;

import rx.Observable;

/**
 * Created by sanbhara on 1/4/19.
 */

public interface CountryRepository {

    /**
     * Search Country
     *
     * @param query
     * @return
     */
    Observable<List<Country>> search(String query);
}
