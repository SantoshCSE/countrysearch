package com.santosh.csa.service.impl;


import com.santosh.csa.models.Country;
import com.santosh.csa.service.CountryRetrofitAPIClient;
import com.santosh.csa.utils.AppConstant;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * Created by sanbhara on 1/4/19.
 */

public class CountryRetrofitAPIClientImpl
        implements CountryRetrofitAPIClient {

    private static Retrofit mRetrofit;
    private static CountryRetrofitAPIClient mCountryRetrofitAPIClient;

    public CountryRetrofitAPIClientImpl() {
        setUpRetrofit();
        createServices();
    }

    @Override
    public Observable<List<Country>> search(String name) {
        return mCountryRetrofitAPIClient.search(name);
    }

    private void setUpRetrofit() {
        mRetrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(AppConstant.BASE_URL)
                .build();
    }

    private void createServices() {
        mCountryRetrofitAPIClient = mRetrofit.create(CountryRetrofitAPIClient.class);
    }
}
