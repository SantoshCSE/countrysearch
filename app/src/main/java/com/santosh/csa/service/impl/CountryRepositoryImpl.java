package com.santosh.csa.service.impl;

import com.santosh.csa.models.Country;
import com.santosh.csa.service.CountryRepository;
import com.santosh.csa.service.CountryRetrofitAPIClient;

import java.util.List;

import rx.Observable;

/**
 * Created by sanbhara on 1/4/19.
 */

public class CountryRepositoryImpl implements CountryRepository {

    private CountryRetrofitAPIClient mCountryRetrofitAPIClient;

    public CountryRepositoryImpl(CountryRetrofitAPIClient countryRepositoryRemote) {
        this.mCountryRetrofitAPIClient = countryRepositoryRemote;
    }

    @Override
    public Observable<List<Country>> search(String query) {
        return mCountryRetrofitAPIClient.search(query);
    }
}
