package com.santosh.csa.service;


import com.santosh.csa.models.Country;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by sanbhara on 1/4/19.
 */

public interface CountryRetrofitAPIClient {

    @GET("name/{name}")
    Observable<List<Country>> search(@Path("name") String name);
}
