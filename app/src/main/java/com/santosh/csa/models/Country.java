package com.santosh.csa.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by sanbhara on 1/4/19.
 */
@Entity(tableName = "country_table", indices = {@Index(value = "capital", unique = true)})
public class Country implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    @Expose(serialize = false, deserialize = false)
    public int id;
    private String name;
    @Ignore
    private List<String> callingCodes;
    private String capital;
    private String region;
    private String subregion;
    private String population;
    private String demonym;
    @Ignore
    private List<String> timezones;
    private String nativeName;
    @Ignore
    private List<Currency> currencies;
    @Ignore
    private List<Language> languages;
    private String flag;

    public Country() {

    }

    public Country(String name, List<String> callingCodes, String capital,
            String region, String subregion, List<String> timezones,
            List<Currency> currencies, List<Language> languages,
            String flag) {
        this.name = name;
        this.callingCodes = callingCodes;
        this.capital = capital;
        this.region = region;
        this.subregion = subregion;
        this.timezones = timezones;
        this.currencies = currencies;
        this.languages = languages;
        this.flag = flag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCallingCodes() {
        return callingCodes;
    }

    public void setCallingCodes(List<String> callingCodes) {
        this.callingCodes = callingCodes;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSubregion() {
        return subregion;
    }

    public void setSubregion(String subregion) {
        this.subregion = subregion;
    }

    public List<String> getTimezones() {
        return timezones;
    }

    public void setTimezones(List<String> timezones) {
        this.timezones = timezones;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getDemonym() {
        return demonym;
    }

    public void setDemonym(String demonym) {
        this.demonym = demonym;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    @Override
    public String toString() {
        return "Country{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", callingCodes='" + callingCodes + '\'' +
                ", capital='" + capital + '\'' +
                ", region='" + region + '\'' +
                ", subregion=" + subregion +
                ", population=" + population +
                ", demonym=" + demonym +
                ", timezones='" + timezones + '\'' +
                ", nativeName=" + nativeName +
                ", currencies=" + currencies +
                ", languages='" + languages + '\'' +
                ", flag='" + flag + '\'' +
                '}';
    }

    protected Country(Parcel in) {
        name = in.readString();
        callingCodes = in.createStringArrayList();
        capital = in.readString();
        region = in.readString();
        subregion = in.readString();
        population = in.readString();
        demonym = in.readString();
        timezones = in.createStringArrayList();
        nativeName = in.readString();
        flag = in.readString();
        currencies = in.createTypedArrayList(Currency.CREATOR);
        languages = in.createTypedArrayList(Language.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeStringList(callingCodes);
        parcel.writeString(capital);
        parcel.writeString(region);
        parcel.writeString(subregion);
        parcel.writeString(population);
        parcel.writeString(demonym);
        parcel.writeStringList(timezones);
        parcel.writeString(nativeName);
        parcel.writeString(flag);
        parcel.writeTypedList(currencies);
        parcel.writeTypedList(languages);
    }

    public static final Creator<Country> CREATOR = new Creator<Country>() {
        @Override
        public Country createFromParcel(Parcel in) {
            return new Country(in);
        }

        @Override
        public Country[] newArray(int size) {
            return new Country[size];
        }
    };
}
