package com.santosh.csa.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sanbhara on 1/4/19.
 */
@Entity(tableName = "language_table",
        primaryKeys = {"language_id"},
        foreignKeys = @ForeignKey(entity = Country.class,
                parentColumns = "id",
                childColumns = "language_id"))
public class Language implements Parcelable {

    @ColumnInfo(name = "language_id")
    private long languageId;
    private String name;
    private String nativeName;

    public Language(String name, String nativeName) {
        this.name = name;
        this.nativeName = nativeName;
    }

    public long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    protected Language(Parcel in) {
        name = in.readString();
        nativeName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(nativeName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Language> CREATOR = new Creator<Language>() {
        @Override
        public Language createFromParcel(Parcel in) {
            return new Language(in);
        }

        @Override
        public Language[] newArray(int size) {
            return new Language[size];
        }
    };
}
