package com.santosh.csa.presenter;

/**
 * Created by sanbhara on 1/5/19.
 */
public interface CountryPresenter {

    /**
     * Method to search Country
     */
    void search(String query);

    /**
     * release the resource
     */
    void cleanUp();
}
