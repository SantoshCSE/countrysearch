package com.santosh.csa.presenter.impl;


import com.santosh.csa.listener.CountryPresenterView;
import com.santosh.csa.presenter.CountryPresenter;
import com.santosh.csa.service.CountryRepository;
import com.santosh.csa.service.impl.CountryRepositoryImpl;
import com.santosh.csa.service.impl.CountryRetrofitAPIClientImpl;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sanbhara on 1/5/19.
 */
public class CountryPresenterImpl implements CountryPresenter {

    private CountryPresenterView mCountryPresenterView;
    private CountryRepository mCountryRepository;
    private Subscription mCountrySubscription;

    private String mQuery;

    public CountryPresenterImpl(CountryPresenterView view) {
        this.mCountryPresenterView = view;
        mCountryRepository = new CountryRepositoryImpl(new CountryRetrofitAPIClientImpl());
    }

    @Override
    public void search(String query) {
        this.mQuery = query;
        if (mCountrySubscription != null && !mCountrySubscription.isUnsubscribed())
            mCountrySubscription.unsubscribe();
        mCountrySubscription = mCountryRepository.search(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(() -> mCountryPresenterView.clearDataShowLoading())
                .doOnCompleted(() -> mCountryPresenterView.dismissLoadAndNotify())
                .subscribe((countries) -> mCountryPresenterView.addDataToCountryList(countries), (throwable) -> mCountryPresenterView.handleErrorScenario(throwable));
    }

    @Override
    public void cleanUp() {
        if (mCountrySubscription != null && !mCountrySubscription.isUnsubscribed())
            mCountrySubscription.unsubscribe();
    }

}
