package com.santosh.csa.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.santosh.csa.models.Currency;

import java.util.List;

/**
 * Created by sanbhara on 1/5/19.
 */

@Dao
public interface CurrencyDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertCurrencies(List<Currency> currencies);

    @Query("SELECT * FROM currency_table WHERE currency_id = :currencyId")
    List<Currency> getCurrencyById(long currencyId);
}
