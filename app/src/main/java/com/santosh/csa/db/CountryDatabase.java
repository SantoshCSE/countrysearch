package com.santosh.csa.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.santosh.csa.models.Country;
import com.santosh.csa.models.Currency;
import com.santosh.csa.models.Language;

/**
 * Created by sanbhara on 1/5/19.
 */
@Database(entities = {Country.class, Currency.class, Language.class}, version = 1, exportSchema = false)
public abstract class CountryDatabase extends RoomDatabase{

    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "country_db";
    private static CountryDatabase sInstance;

    public static CountryDatabase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        CountryDatabase.class, DATABASE_NAME)
                        .build();
            }
        }
        return sInstance;
    }


    public abstract CountryDao countryDAO();
    public abstract CurrencyDao currencyDAO();
    public abstract LanguageDao languageDao();
}
