package com.santosh.csa.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.santosh.csa.models.Country;

import java.util.List;

/**
 * Created by sanbhara on 1/5/19.
 */

@Dao
public interface CountryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Country country);

    @Query("SELECT * from country_table")
    List<Country> getSavedCountryList();

    @Query("SELECT * from country_table WHERE name LIKE :search")
    LiveData<List<Country>> getCountryList(String search);
}
