package com.santosh.csa.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.santosh.csa.models.Language;

import java.util.List;

/**
 * Created by sanbhara on 1/5/19.
 */

@Dao
public interface LanguageDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertLanguages(List<Language> languages);

    @Query("SELECT * FROM language_table WHERE language_id = :languageId")
    List<Language> getLanguageById(long languageId);
}
