package com.santosh.csa.db;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import com.santosh.csa.models.Country;
import com.santosh.csa.models.Currency;
import com.santosh.csa.models.Language;
import com.santosh.csa.utils.AppExecutors;

import java.util.List;

/**
 * Created by sanbhara on 1/5/19.
 */

public class CountryDataSource {
    private final CountryDao countryDao;
    private final CurrencyDao currencyDao;
    private final LanguageDao languageDao;
    private static CountryDataSource sInstance;
    private static final Object LOCK = new Object();
    private final AppExecutors mExecutor;

    private CountryDataSource(Context context) {
        countryDao = CountryDatabase.getInstance(context).countryDAO();
        currencyDao = CountryDatabase.getInstance(context).currencyDAO();
        languageDao = CountryDatabase.getInstance(context).languageDao();
        mExecutor = AppExecutors.getInstance();
    }

    public synchronized static CountryDataSource getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new CountryDataSource(context);
            }
        }
        return sInstance;
    }

    public List<Country> getCountyList() {
        List<Country> countryList = countryDao.getSavedCountryList();
        for (Country country : countryList) {
            List<Currency> currencies = currencyDao.getCurrencyById(country.getId());
            country.setCurrencies(currencies);
        }
        for (Country country : countryList) {
            List<Language> languages = languageDao.getLanguageById(country.getId());
            country.setLanguages(languages);
        }
        return countryList;
    }

    public LiveData<List<Country>> getCountryList(String search){
        return countryDao.getCountryList(search);
    }

    public void saveCounty(Country country) {
        mExecutor.getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                long rowId = countryDao.insert(country);
                List<Currency> currencies = country.getCurrencies();
                if (currencies != null) {
                    for (Currency currency : currencies) {
                        currency.setCurrencyId(rowId);
                    }
                    currencyDao.insertCurrencies(currencies);
                }
                List<Language> languages = country.getLanguages();
                if (languages != null) {
                    for (Language language : languages) {
                        language.setLanguageId(rowId);
                    }
                    languageDao.insertLanguages(languages);
                }
            }
        });
    }
}
