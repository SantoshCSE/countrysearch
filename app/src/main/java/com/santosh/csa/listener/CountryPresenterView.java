package com.santosh.csa.listener;


import com.santosh.csa.models.Country;

import java.util.List;

/**
 * Created by sanbhara on 1/4/19.
 */
public interface CountryPresenterView {
    /**
     * Clear the county list and show loading
     */
    void clearDataShowLoading();

    /**
     * Hide the progress and notify
     */
    void dismissLoadAndNotify();

    /**
     * Add search result to list
     *
     * @param countries
     */
    void addDataToCountryList(List<Country> countries);

    /**
     * Handle error
     *
     * @param throwable
     */
    void handleErrorScenario(Throwable throwable);
}
