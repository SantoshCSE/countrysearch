package com.santosh.csa.app;

import android.app.Application;

/**
 * Created by sanbhara on 1/4/19.
 */
public class CSApplication extends Application {
    public static final String TAG = CSApplication.class.getSimpleName();
    public static CSApplication app;

    public CSApplication() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }
}
